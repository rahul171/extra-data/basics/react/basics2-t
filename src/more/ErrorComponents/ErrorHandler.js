import React from 'react';

export default class ErrorHandler extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: ''
        }
    }

    componentDidCatch(error, errorInfo) {
        this.setState({ error });
        console.log(error);
        console.log(errorInfo);
    }

    render() {
        if (!this.state.error) {
            return this.props.children;
        }

        return (
            <div className="error-handler">
                <div>{this.state.error}</div>
            </div>
        );
    }
}
