import React from 'react';
import ErrorHandler from "./ErrorHandler";

export default class ErrorComponent1 extends React.Component {
    render() {
        return (
            <ErrorHandler>
                <div>error component 1</div>
                <div>{throw new Error('errrrrr')}</div>
            </ErrorHandler>
        );
    }
}
