import React from 'react';
import ErrorComponent1 from "./ErrorComponent1";

export default class ErrorComponentsWrapper extends React.Component {
    render() {
        return (
            <div className="error-components-wrapper">
                <ErrorComponent1 />
            </div>
        );
    }
}
