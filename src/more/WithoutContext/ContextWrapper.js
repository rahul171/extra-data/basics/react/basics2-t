import React from 'react';
import Context1 from "./Context1";

export default class ContextWrapper extends React.Component {
    render() {
        return (
            <div className="ContextWrapper">
                <Context1 />
            </div>
        );
    }
}
