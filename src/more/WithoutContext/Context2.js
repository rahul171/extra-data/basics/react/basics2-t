import React from 'react';

export default class Context2 extends React.Component {
    onDeepDown = (e) => {
        this.props.onDeepDown(e.target.value);
    }

    render() {
        return (
            <div className="Context2">
                <div>
                    Context2
                </div>
                <div>
                    <div>{this.props.msg}</div>
                    <input type="text" defaultValue={this.props.msg} onChange={this.onDeepDown} />
                </div>
            </div>
        );
    }
}
