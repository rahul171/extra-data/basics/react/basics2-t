import React from 'react';
import Middleware3 from "./Middleware3";

export default class Middleware2 extends React.Component {
    render() {
        return (
            <div className="Middleware2">
                <Middleware3 msg={this.props.msg} onDeepDown={this.props.onDeepDown} />
            </div>
        );
    }
}
