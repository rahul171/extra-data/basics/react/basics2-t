import React from 'react';
import Middleware2 from "./Middleware2";

export default class Middleware1 extends React.Component {
    render() {
        return (
            <div className="Middleware1">
                <Middleware2 msg={this.props.msg} onDeepDown={this.props.onDeepDown} />
            </div>
        );
    }
}
