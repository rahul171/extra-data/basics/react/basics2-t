import React from 'react';
import Context2 from "./Context2";

export default class Middleware3 extends React.Component {
    render() {
        return (
            <div className="Middleware3">
                <Context2 msg={this.props.msg} onDeepDown={this.props.onDeepDown} />
            </div>
        );
    }
}
