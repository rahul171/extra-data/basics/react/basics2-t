import React from 'react';
import Middleware1 from "./Middleware1";

export default class Context1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            msg: 'msg',
            fromDeepDown: 'something'
        }
    }

    handleMsgChange = (e) => {
        this.setState({ msg: e.target.value });
    }

    handleDeepDown = (fromDeepDown) => {
        this.setState({ fromDeepDown });
    }

    render() {
        return (
            <div className="Context1">
                <div>
                    <Middleware1 msg={this.state.msg} onDeepDown={this.handleDeepDown} />
                </div>
                <hr />
                <div>
                    <div>{this.state.fromDeepDown}</div>
                    <input type="text" value={this.state.msg} onChange={this.handleMsgChange} />
                </div>
            </div>
        );
    }
}
