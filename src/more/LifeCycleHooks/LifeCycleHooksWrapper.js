import React from 'react';
// import LifeCycleHook1Wrapper from "./LifeCycleHook1Wrapper";
import FunctionComponentLH1Wrapper from "./FunctionComponentLH1Wrapper";
import FunctionComponentLH2Wrapper from "./FunctionComponentLH2Wrapper";

export default class LifeCycleHooksWrapper extends React.Component {
    render() {
        return (
            <div className="LifeCycleHooksWrapper">
                {/*<LifeCycleHook1Wrapper /><hr />*/}
                <FunctionComponentLH1Wrapper /><hr />
                {/*<FunctionComponentLH2Wrapper />*/}
            </div>
        );
    }
}
