import React from 'react';
import {line, getData, plusPlus, testVar, plusPlusTestVar, logTestVar} from './helper';

// this is called only 1 time, on the next consecutive component unmount/mount, class will already be there,
// i.e all the above import will also be there already, only the class constructor and class methods (lifecycle hooks)
// will be called again. since the previous imports will already be there, the previous instances will be used.
console.log('the outer scope');
line();

// this code outer side of the class will only be called on import.
// so the 'the outer scope' log will only be logged on import. (doesn't matter if the component is used or not.)

/*
    The flow on update:

        getDerivedStateFromProps()
        shouldComponentUpdate()
        render()
        getSnapshotBeforeUpdate()
        componentDidUpdate()


    The flow on mount:

        constructor()
        getDerivedStateFromProps()
        render()
        componentDidMount()


    The flow on unmount:

        componentWillUnmount()


    Any of the above flow won't be called if the component is not in the render method of the parent component.
    i.e. only importing the component or assigning it to a variable won't initialize any flow.
    only when being rendered in the parent component, the flow will be executed.
    during this execution, the parent flow will be at halt, it will wait for the child flow to complete.
 */

export default class LifeCycleHook1 extends React.Component {

    constructor(props) {
        super(props);

        console.log('constructor');

        this.state = {
            // name will reset on component unmount/mount
            name: 'value0',
            // this won't reset because the data is being stored in the helper.js,
            // on component unmount, the data in the helper.js stays as it is.
            // when this component mounts again, we import helper.js, and access the previous data
            // why? read the comment above the 'the outer scope' log.
            ...getData('state')
        };

        this.logPropsState();

        console.log('testVar', testVar);
        // can't increment the value from here, because when imported, it only gives getter, not setter.
        // console.log('++', ++testVar);
        // this will use previous values, not a new one.
        // why? read the comment above the 'the outer scope' log.
        logTestVar();
        plusPlusTestVar();
        logTestVar();

        line();
    }

    changeName = () => {
        console.log('changeName');
        this.logPropsState();
        line();
        this.setState(state => ({ name: plusPlus(this.state.name) }));
    }

    componentDidMount() {
        console.log('componentDidMount');
        line();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate');
        console.log('prevProps', prevProps);
        console.log('prevState', prevState);
        console.log('snapshot', snapshot);
        this.logPropsState();
        line();
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
        line();
    }

    componentDidCatch(error, errorInfo) {
        console.log('componentDidCatch');
        console.log('error', error);
        console.log('errorInfo', errorInfo);
        line();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('shouldComponentUpdate');
        console.log('nextProps', nextProps);
        console.log('nextState', nextState);
        this.logPropsState();
        console.log('nextContext', nextContext);
        line();

        return true;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('getSnapshotBeforeUpdate');
        console.log('prevProps', prevProps);
        console.log('prevState', prevState);
        this.logPropsState();
        const returnData = getData('getSnapshotBeforeUpdate');
        line();
        return returnData;
    }

    static getDerivedStateFromProps(props, state) {
        console.log('getDerivedStateFromProps');
        console.log('props', props);
        console.log('state', state);
        const returnData = {...getData('getDerivedStateFromProps'), ...getData('state')};
        line();
        return returnData;
    }

    static getDerivedStateFromError() {
        console.log('getDerivedStateFromError');
        console.log(arguments);
        const returnData =  getData('getDerivedStateFromError');
        line();
        return returnData;
    }

    logProps = () => {
        console.log('currentProps', this.props);
    }

    logState = () => {
        console.log('currentState', this.state);
    }

    logPropsState = () => {
        this.logProps();
        this.logState();
    }

    render() {

        console.log('render');
        this.logPropsState();
        line();

        return (
            <div className="LifeCycleHook1">
                LifeCycleHook1
                <div>
                    <button onClick={this.changeName}>change name</button>
                </div>
            </div>
        );
    }
}
