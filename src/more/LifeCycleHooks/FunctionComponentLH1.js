import React, { useEffect } from 'react';

const FunctionComponentLH1 = (props) => {
    console.log('hello1');

    useEffect(() => {
        console.log('useEffect', props);
        return () => {
            console.log('something');
        }
    });

    useEffect(() => {
        console.log('first => useEffect', props);
    }, []);

    useEffect(() => {
        console.log('prop1 => useEffect', props);
    }, [props.prop1]);

    console.log('hello2');

    return (
        <div>
            {console.log('inside return')}
            FunctionComponentLH{'{n}'}
        </div>
    );
}

export default FunctionComponentLH1;
