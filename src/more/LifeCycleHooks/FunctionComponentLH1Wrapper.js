import React from 'react';
import FunctionComponentLH1 from "./FunctionComponentLH1";
import { plusPlus } from "./helper";

export default class FunctionComponentLH1Wrapper extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: true,
            prop1: 'prop1-0',
            prop2: 'prop2-0',
            prop3: 'prop3-0',
        }
    }

    changeProp1 = () => {
        this.setState(state => ({ prop1: plusPlus(state.prop1) }));
    }

    changeProp2 = () => {
        this.setState(state => ({ prop2: plusPlus(state.prop2) }));
    }

    changeProp3 = () => {
        this.setState(state => ({ prop3: plusPlus(state.prop3) }));
    }

    reRender = () => {
        this.setState({...this.state});
    }

    toggle = () => {
        this.setState(state => ({ show: !state.show }));
    }

    render() {
        return (
            <div className="FunctionComponentLH1Wrapper">
                <div>
                    FunctionComponentLH1Wrapper
                </div>
                <div>
                    <button onClick={this.reRender}>re-render without change</button>
                    <button onClick={this.toggle}>toggle</button>
                    <button onClick={this.changeProp1}>change prop1</button>
                    <button onClick={this.changeProp2}>change prop2</button>
                    <button onClick={this.changeProp3}>change prop3</button>
                </div>
                <hr />
                {this.state.show &&
                    <FunctionComponentLH1
                        prop1={this.state.prop1}
                        prop2={this.state.prop2}
                        prop3={this.state.prop3}
                    />
                }
            </div>
        );
    }
}
