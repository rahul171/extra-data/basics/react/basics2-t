export const line = (value = '') => {
    // console.log(`-----------${value}------------`);
    console.log(' ');
}

export const plusPlus = (value) => {
    if (typeof value !== 'string') {
        return value;
    }

    return value.slice(0, value.length - 1) + (parseInt(value.slice(-1)) + 1);
}

const data = {};

export const getData = (key) => {

    if (!data[key]) {
        data[key] = 'value0';
    } else {
        data[key] = plusPlus(data[key]);
    }

    console.log('====================================>', {...data});

    return {
        [key]: data[key]
    }
}

export let testVar = 1;

export const plusPlusTestVar = () => { testVar++; }

export const logTestVar = () => { console.log(testVar); };
