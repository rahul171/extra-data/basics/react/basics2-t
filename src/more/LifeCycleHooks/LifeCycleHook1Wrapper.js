import React from 'react';
import LifeCycleHook1 from "./LifeCycleHook1";

export default class LifeCycleHook1Wrapper extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: true,
            msg: 'hello0'
        }
    }

    changeMsg = () => {
        this.setState(state => ({
            msg: state.msg.slice(0, state.msg.length - 1) + (parseInt(state.msg.slice(-1)) + 1)
        }));
    }

    reRender = () => {
        this.setState({...this.state});
    }

    toggle = () => {
        this.setState(state => ({ show: !state.show }));
    }

    render() {
        return (
            <div className="LifeCycleHook1Wrapper">
                <div>
                    <button onClick={this.reRender}>re-render without change</button>
                    <button onClick={this.toggle}>toggle</button>
                    <button onClick={this.changeMsg}>change msg</button>
                </div>
                <hr />
                {this.state.show &&
                    <LifeCycleHook1 msg={this.state.msg} />
                }
            </div>
        );
    }
}
