import React from 'react';

export default class ClassComponent extends React.Component {
    constructor(props) {
        super(props);

        this.testThis3 = this.testThis3.bind(this);
    }

    // 'this' is bound here, and can't be changed with .bind method.
    testThis = () => {
        console.log(this);
    }

    // can change 'this' with .bind method
    testThis2() {
        console.log(this);
    }

    // pre .bind in the constructor.
    // once .bind is used, after that, can't change 'this' again on the bounded method.
    testThis3() {
        console.log(this);
    }

    render() {
        return (
            <div className="class-component">
                class component <br />
                <button onClick={this.testThis}>test this</button>
                {/* this doesn't even if bind is used in this case. */}
                <button onClick={this.testThis.bind({name: 'hello'})}>test this with bind</button>
                <button onClick={this.testThis2}>test this 2 without wrapper</button>
                <button onClick={() => { this.testThis2() }}>test this 2 with wrapper</button>
                <button onClick={this.testThis2.bind({name: 'hello'})}>test this 2 with bind</button>
                <button onClick={this.testThis3}>test this 3 (pre bind)</button>
                {/* once .bind is used, after that, can't change 'this' again on the bounded method */}
                <button onClick={this.testThis3.bind({name: 'hello'})}>test this 3 (pre bind, using .bind again)</button>
            </div>
        );
    }
}
