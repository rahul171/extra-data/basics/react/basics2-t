import React from 'react';
import ClassComponent from './ClassComponent';

export default class ClassComponentWrapper extends React.Component {
    render() {
        return (
            <div className="class-component-wrapper">
                <ClassComponent />
            </div>
        );
    }
}
