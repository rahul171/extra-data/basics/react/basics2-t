import React from 'react';
import FunctionComponent from './FunctionComponent';
import FunctionComponentStateHook from "./FunctionComponentStateHook";
import MultipleStates from "./MultipleStates";

const functionComponentWrapper = (props) => {
    console.log('in in');
    return (
        <div className="function-component-wrapper">
            <FunctionComponent /><hr />
            <FunctionComponentStateHook /><hr />
            <MultipleStates />
        </div>
    );
}

export default functionComponentWrapper;
