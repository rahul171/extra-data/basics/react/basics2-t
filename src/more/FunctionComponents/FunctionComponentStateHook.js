import React, { useState } from 'react';

export default (props) => {
    const [state, setState] = useState({
        name: 'hello0',
        value: 10
    });

    const plusPlus = (value) => {
        if (typeof value === 'number') {
            return value + 1;
        }
        return value.slice(0, value.length - 1) + (parseInt(value.slice(-1)) + 1);
    }

    const change = () => {
        setState({
            name: plusPlus(state.name)
        });
    }

    const changeWithMerge = () => {
        setState({
            ...state,
            name: plusPlus(state.name)
        });
    }

    const logState = () => {
        console.log(state);
    }

    return (
        <div className="function-component-with-state-hook">
            one state <br />
            <button onClick={change}>change</button>
            <button onClick={changeWithMerge}>change with merge</button>
            <button onClick={logState}>log state</button>
        </div>
    );
};
