import React, { useState } from 'react';

const Abcd = function(props) {
    const [nameState, setNameState] = useState({ name: 'hello0' });
    const [valueState, setValueState] = useState({ value: 10 });

    const plusPlus = (value) => {
        if (typeof value === 'number') {
            return value + 1;
        }
        return value.slice(0, value.length - 1) + (parseInt(value.slice(-1)) + 1);
    }

    const changeName = () => {
        setNameState({
            name: plusPlus(nameState.name)
        });
    }

    const changeValue = () => {
        setValueState({
            value: plusPlus(valueState.value)
        });
    }

    const logState = () => {
        console.log(nameState);
        console.log(valueState);
    }

    return (
        <div className="multiple-states">
            multiple states <br />
            {/* see the comments below on the .prototype property */}
            {/*{this.something()}<br />*/}
            <button onClick={changeName}>change name</button>
            <button onClick={changeValue}>change value</button>
            <button onClick={logState}>log state</button>
        </div>
    );
};

// The function component is not invoked with a new keyword. so this won't work.
// Abcd.prototype.something = () => {
//     console.log('works?');
// }

export default Abcd;
