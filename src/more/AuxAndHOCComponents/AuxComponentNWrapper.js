import React from 'react';
import AuxComponent1 from "./AuxComponent1";
import AuxComponent2 from "./AuxComponent2";
import AuxComponent3 from "./AuxComponent3";
import AuxComponent4 from "./AuxComponent4";

export default class AuxComponentNWrapper extends React.Component {
    render() {
        return (
            <div className="AuxComponent1Wrapper">
                <AuxComponent1 /><hr />
                <AuxComponent2 /><hr />
                <AuxComponent3 name="hello" /><hr />
                <AuxComponent4 />
            </div>
        );
    }
}
