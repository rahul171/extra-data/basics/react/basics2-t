import React from 'react';

export default class AuxComponent1 extends React.Component {
    render() {
        return (
            [
                <div key="item-1">element1</div>,
                <div key="item-2">element2</div>,
                <div key="item-3">element3</div>
            ]
        );
    }
}
