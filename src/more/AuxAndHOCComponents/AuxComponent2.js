import React from 'react';
import Aux1 from "./Aux1";

export default class AuxComponent2 extends React.Component {
    render() {
        return (
            <Aux1>
                <div>element1</div>
                <div>element2</div>
                <div>element3</div>
            </Aux1>
        );
    }
}
