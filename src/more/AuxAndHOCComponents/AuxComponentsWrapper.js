import React from 'react';
import AuxComponentNWrapper from "./AuxComponentNWrapper";

export default class AuxComponentsWrapper extends React.Component {
    render() {
        return (
            <div className="AuxComponentsWrapper">
                <AuxComponentNWrapper />
            </div>
        );
    }
}
