import React from 'react';

export default class AuxComponent4 extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div>element1</div>
                <div>element2</div>
                <div>element3</div>
            </React.Fragment>
        );
    }
}
