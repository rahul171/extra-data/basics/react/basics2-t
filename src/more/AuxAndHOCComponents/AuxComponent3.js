import React from 'react';
import hoc1 from "./HOC1";
import Aux1 from "./Aux1";

class AuxComponent3 extends React.Component {
    render() {
        return (
            <Aux1>
                <div>element1</div>
                <div>element2</div>
                <div>element3</div>
                <div>{this.props.name}</div>
            </Aux1>
        );
    }
}

export default hoc1(AuxComponent3);
