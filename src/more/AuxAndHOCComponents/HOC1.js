import React from 'react';

export default (Component) => {
    return props => {
        return (
            <div>
                <Component {...props} />
            </div>
        );
    }
}
