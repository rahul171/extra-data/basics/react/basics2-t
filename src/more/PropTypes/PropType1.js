import React  from 'react';
import PropType from 'prop-types';

class PropType1 extends React.Component {
    render() {
        return (
            <div className="PropType1">
                PropType1
            </div>
        );
    }
}

PropType1.propTypes = {
    name: PropType.string,
    abcd: PropType.func
}

export default PropType1;
