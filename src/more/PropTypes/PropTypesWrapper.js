import React from 'react';
import PropType1 from "./PropType1";

export default class PropTypesWrapper extends React.Component {
    render() {
        return (
            <div className="PropTypesWrapper">
                {/* see browser console */}
                <PropType1 name={33} abcd={() => {}} c={1} />
            </div>
        );
    }
}
