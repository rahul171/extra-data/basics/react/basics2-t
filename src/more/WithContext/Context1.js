import React from 'react';
import Middleware1 from "./Middleware1";
import CreateContext from "./CreateContext";

import css from './context1.module.css';

export default class Context1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            msg: 'msg',
            fromDeepDown: 'something'
        }
    }

    handleMsgChange = (e) => {
        this.setState({ msg: e.target.value });
    }

    handleDeepDown = (fromDeepDown) => {
        this.setState({ fromDeepDown });
    }

    render() {
        return (
            <div className={css.error}>
                <div>
                    <CreateContext.Provider value={{
                        fromDeepDown: this.state.fromDeepDown,
                        onDeepDown: this.handleDeepDown
                    }}>
                        <Middleware1 />
                    </CreateContext.Provider>
                </div>
                <hr />
                <div>
                    <div>{this.state.fromDeepDown}</div>
                    {/* Changing input here doesn't propagate to child component when using context */}
                    <input
                        type="text"
                        value={this.state.fromDeepDown}
                        onChange={(e) => { this.handleDeepDown(e.target.value) }}
                    />
                </div>
            </div>
        );
    }
}
