import React from 'react';
import CreateContext from "./CreateContext";

export default class Context3 extends React.Component {

    static contextType = CreateContext;

    render() {
        return (
            <div className="Context3">
                <div>
                    Context3
                </div>
                <div>
                    <div>{this.context.msg}</div>
                    <input
                        type="text"
                        defaultValue={this.context.fromDeepDown}
                        onChange={(e) => { this.context.onDeepDown(e.target.value); }}
                    />
                </div>
            </div>
        );
    }
}
