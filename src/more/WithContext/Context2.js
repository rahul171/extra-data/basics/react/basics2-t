import React from 'react';
import CreateContext from "./CreateContext";

export default class Context2 extends React.Component {

    render() {
        return (
            <CreateContext.Consumer>
                {(context) => {
                    return (
                        <div className="Context2">
                            <div>
                                Context2
                            </div>
                            <div>
                                <div>{context.msg}</div>
                                <input
                                    type="text"
                                    defaultValue={context.fromDeepDown}
                                    onChange={(e) => { context.onDeepDown(e.target.value); }}
                                />
                            </div>
                        </div>
                    );
                }}
            </CreateContext.Consumer>
        );
    }
}
