import React from 'react';
import Context2 from "./Context2";
import Context3 from "./Context3";
import Context4 from "./Context4";

export default class Middleware3 extends React.Component {
    render() {
        return (
            <div className="Middleware3">
                <Context2 /><hr />
                <Context3 /><hr />
                <Context4 />
            </div>
        );
    }
}
