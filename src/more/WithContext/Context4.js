import React, { useContext } from 'react';
import CreateContext from "./CreateContext";

export default (props) => {
    const con4Context = useContext(CreateContext);

    return (
        <div className="Context4">
            <div>
                Context4
            </div>
            <div>
                <div>{con4Context.msg}</div>
                <input
                    type="text"
                    defaultValue={con4Context.fromDeepDown}
                    onChange={(e) => { con4Context.onDeepDown(e.target.value); }}
                />
            </div>
        </div>
    );
}
