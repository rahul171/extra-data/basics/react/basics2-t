import React from 'react';
import css from './CSSModule2.module.css';

export default class CSSModule2 extends React.Component {
    render() {
        console.log(css);
        return (
            <div className="css-module-2">
                css module 2
            </div>
        );
    }
}
