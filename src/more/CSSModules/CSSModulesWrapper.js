import React from 'react';
import CSSModule1 from "./CSSModule1";
import CSSModule2 from "./CSSModule2";

export default class CSSModulesWrapper extends React.Component {
    render() {
        return (
            <div className="css-modules-wrapper">
                <CSSModule1 /><hr />
                <CSSModule2 />
            </div>
        );
    }
}
