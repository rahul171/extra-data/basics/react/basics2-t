import React from 'react';
import css from './CSSModule1.module.css';

export default class CSSModule1 extends React.Component {
    render() {
        console.log(css);
        return (
            <div className="css-module-1">
                css module 1
                <div className="abccdd">limitation of css module</div>
            </div>
        );
    }
}
