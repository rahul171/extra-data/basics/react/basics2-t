import React, { useEffect, useRef } from 'react';

export default (props) => {
    const abcd = useRef(null);
    const abcd1 = useRef(null);

    useEffect(() => {
        console.log(abcd.current.innerHTML);
        console.log(abcd1.current.value);
    });

    return (
        <div>
            <div>Ref1</div>
            <div ref={abcd}>hello there</div>
            <input type="text" defaultValue="something" ref={abcd1} />
        </div>
    );
}
