import React from 'react';
import Ref1 from "./Ref1";

export default class RefsWrapper extends React.Component {
    render() {
        return (
            <div className="RefsWrapper">
                <Ref1 />
            </div>
        );
    }
}
