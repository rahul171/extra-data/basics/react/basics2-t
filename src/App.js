import React from 'react';
import './App.css';
import FunctionComponentWrapper from './more/FunctionComponents/FunctionComponentWrapper';
import ClassComponentWrapper from './more/ClassComponents/ClassComponentWrapper';
import CSSModulesWrapper from "./more/CSSModules/CSSModulesWrapper";
// import LifeCycleHooksWrapper from "./more/LifeCycleHooks/LifeCycleHooksWrapper";
import AuxComponentsWrapper from "./more/AuxAndHOCComponents/AuxComponentsWrapper";
import PropTypesWrapper from "./more/PropTypes/PropTypesWrapper";
import RefsWrapper from "./more/Refs/RefsWrapper";
import ContextWrapper from "./more/WithoutContext/ContextWrapper";
import ContextWrapper2 from "./more/WithContext/ContextWrapper";
// import ErrorComponentsWrapper from "./more/ErrorComponents/ErrorComponentsWrapper";

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            components: [
                {
                    value: <FunctionComponentWrapper />,
                    show: false
                },
                {
                    value: <ClassComponentWrapper />,
                    show: false
                },
                {
                    value: <CSSModulesWrapper />,
                    show: false
                },
                // {
                //     value: () => { return <ErrorComponentsWrapper /> },
                //     show: false
                // }
                // {
                //     value: <LifeCycleHooksWrapper />,
                //     show: false
                // },
                {
                    value: <AuxComponentsWrapper />,
                    show: false
                },
                {
                    value: <PropTypesWrapper />,
                    show: false
                },
                {
                    value: <RefsWrapper />,
                    show: false
                },
                {
                    value: <ContextWrapper />,
                    show: false
                },
                {
                    value: <ContextWrapper2 />,
                    show: true
                }
            ]
        };
    }

    getComponents = () => {
        return this.state.components
            .filter(component => component.show)
            .map((component, index) => (
                <div
                    key={index}
                    className="app-component"
                >
                    {component.value}
                </div>
            ));
    }

    render() {
        return (
            <div className="app">
                {this.getComponents()}
            </div>
        );
    }
}
